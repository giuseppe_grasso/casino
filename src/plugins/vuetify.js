// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify, {
  VCard, VFlex
} from 'vuetify/lib'

Vue.use(Vuetify, {
  components: {
    VCard,
    VFlex
  }
})

const opts = {
  theme: {
    dark: true
  }
}

export default new Vuetify(opts)