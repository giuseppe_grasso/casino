export const userService = {
    login,
    logout
}

//fake login 
function login(username, password) {
    return new Promise((resolve, reject) => {
        if(username==='8001' && password==='MTkyZmQ1NzQ5ODY4NzAxNzdkNTUzODU1'){
            //this is the response from the login service
            let user = {
                username: username
                //other user data
            }
            //handle success
            user.authdata = window.btoa(username + ':' + password)
            localStorage.setItem('user', JSON.stringify(user))
            resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(user)) });
        }
        else{
            reject('Username or password is incorrect');
        }
        return
    })       
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}