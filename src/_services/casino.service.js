import { authHeader } from '../_helpers';
import { userService } from '../_services'
import axios from 'axios'

//for many endpoints depending on different environments should be stored in .env files
const gamesApiUrl = "https://stage-ms.theearplatform.com/v1/casino/games"

export const casinoService = {
    getGames,
    callGame
};

function getGames() {
   return fetchData(gamesApiUrl)
}

function callGame(gameId) {
    const playGameUrl = gamesApiUrl + "/" + gameId + "/play"
    return fetchData(playGameUrl)
}

function  fetchData (apiUrl) {
    return axios({
        baseURL: apiUrl,
        headers: authHeader()
    })
    .then(resp => {
        const data = resp.data.data
        return { ok: true, data: data}
      })
      .catch(err => {   
          if(err.response){
            if (err.response.status == 401) {
                userService.logout();
                return { ok: false, message: err.response.data.message}
            }
            if (err.response.status == 404) {
                return { ok: false, message: err.response.data.message}
            }
            //handle other errors
          }
          else{
              return { ok: false, message: 'Unexpected Error'}
          }
      })
}