import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login.vue'
import CasinoHome from '@/components/CasinoHome.vue'
import GameIFrame from '@/components/GameIFrame.vue'

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/login', name: 'login',  component: Login },
    { path: '/home', name: 'home', component: CasinoHome },
    { path: '/play', name: 'play',  component: GameIFrame, props: true },

    // otherwise redirect to home
    { path: '*', redirect: '/home' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next({ 
      path: '/login', 
      query: { returnUrl: to.path } 
    });
  }
  else next();
})